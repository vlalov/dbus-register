CC=gcc
PREFIX=/usr/local
DOCDIR=${PREFIX}/share/doc/rarcrack

all:
	${CC} -g -I/usr/include/dbus-1.0 -I/usr/lib64/dbus-1.0/include/ -ldbus-1 src/dbus-register.c -O2 -o dbus-register
clean:
	-rm *.o dbus-register
install:
	install -s rarcrack ${PREFIX}/bin
	-mkdir -p ${DOCDIR}
	chmod 755 ${DOCDIR}
#        install -m 644 -t ${DOCDIR} ChangeLog LICENSE README README.html RELEASE_NOTES
uninstall:
	-rm ${PREFIX}/bin/dbus-register
