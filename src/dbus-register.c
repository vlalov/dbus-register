/*
 * dbus-register.c
 *
 *  Created on: Oct 15, 2013
 *      Author: vlalov
 */
#include<dbus/dbus.h>
#include<dbus/dbus-shared.h>
#include<dbus/dbus-protocol.h>
#include<stdio.h>
#include<stdlib.h>

int main(int argc, char **argv) {
	DBusError err;
	DBusConnection* conn;
	int ret;
	// initialise the errors
	dbus_error_init(&err);
	// connect to the bus
	conn = dbus_bus_get(DBUS_BUS_SESSION, &err);
	if (dbus_error_is_set(&err)) {
		fprintf(stderr, "Connection Error (%s)\n", err.message);
		dbus_error_free(&err);
	}
	if (NULL == conn) {
		exit(1);
	}
	// request a name on the bus
	ret = dbus_bus_request_name(conn, "test.method.server",
			DBUS_NAME_FLAG_REPLACE_EXISTING, &err);
	if (dbus_error_is_set(&err)) {
		fprintf(stderr, "Name Error (%s)\n", err.message);
		dbus_error_free(&err);
	}
	if (DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER != ret) {
		exit(1);
	}

	// add a rule for which messages we want to see
	dbus_bus_add_match(conn, "type='signal',interface='org.kde.Solid.PowerManagement',member=brightnessChanged",
			&err); // see signals from the given interface
	dbus_connection_flush(conn);
	if (dbus_error_is_set(&err)) {
		fprintf(stderr, "Match Error (%s)\n", err.message);
		exit(1);
	}

	// loop listening for signals being emmitted
	while (1) {
		DBusMessageIter args;
		// non blocking read of the next available message
		dbus_connection_read_write(conn, 0);
		DBusMessage* msg = dbus_connection_pop_message(conn);

		// loop again if we haven't read a message
		if (NULL == msg) {
			sleep(1);
			continue;
		}

		// check if the message is a signal from the correct interface and with the correct name
		if (dbus_message_is_signal(msg, "org.kde.Solid.PowerManagement", "brightnessChanged")) {
			// read the parameters
			if (!dbus_message_iter_init(msg, &args)) {
				fprintf(stderr, "Message has no arguments!\n");
			} else if (DBUS_TYPE_INT32 == dbus_message_iter_get_arg_type(&args)) {
				int brightness;
				dbus_message_iter_get_basic(&args, &brightness);
				fprintf(stderr, "Brightness: %d\n", brightness);
			}
		}

		// free the message
		dbus_message_unref(msg);
	}
	dbus_connection_close(conn);
	return EXIT_SUCCESS;
}
